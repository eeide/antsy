"""Ubuntu 22 (or other OS) on a d430 (or other node type).

Instructions:
Swap 'er in, gitter done!"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab
# Import Emulab Ansible-specific extensions.
import geni.rspec.emulab.ansible
from geni.rspec.emulab.ansible import (Role, RoleBinding, Override, Playbook)

HEAD_CMD = "sudo -u `geni-get user_urn | cut -f4 -d+` -Hi /bin/sh -c '/local/repository/emulab-ansible-bootstrap/head.sh >/local/logs/setup.log 2>&1'"
CLIENT_CMD = "sudo -u `geni-get user_urn | cut -f4 -d+` -Hi /bin/sh -c '/local/repository/emulab-ansible-bootstrap/client.sh >/local/logs/setup.log 2>&1'"

pc = portal.Context()

pc.defineParameter("IMAGE","Disk Image",
                   portal.ParameterType.STRING, "UBUNTU22-64-STD",
                   [("UBUNTU22-64-STD","Ubuntu 22.04"),
                    ("UBUNTU20-64-STD","Ubuntu 20.04"),
                    ("UBUNTU18-64-STD","Ubuntu 18.04"),
                    ("CENTOS9S-64-STD","CentOS 9S"),
                    ("CENTOS8S-64-STD","CentOS 8S"),
                    ("CENTOS8-64-STD","CentOS 8"),
                    ("CENTOS7-64-STD","CentOS 7"),
                    ("FBSD132-64-STD","FreeBSD 13.2"),
                    ("FBSD131-64-STD","FreeBSD 13.1"),
                    ("FBSD130-64-STD","FreeBSD 13.0"),
                    ("FBSD124-64-STD","FreeBSD 12.4"),
                    ("FBSD123-64-STD","FreeBSD 12.3"),
                    ("FBSD122-64-STD","FreeBSD 12.2"),
                    ("FBSD121-64-STD","FreeBSD 12.1"),
                    ("FBSD114-64-STD","FreeBSD 11.4")])
pc.defineParameter("HWTYPE","Node Type",
                   portal.ParameterType.STRING, "d430",
                   [("d430","d430"),
                    ("d710","d710"),
                    ("d820","d820"),
                    ("pc3000","pc3000"),
                    ("m400", "m400")])
params = pc.bindParameters()
pc.verifyParameters()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Add some custom roles to the rspec; we will bind them to nodes later.
request.addRole(
    Role("setup",path="ansible",playbooks=[
        Playbook("setup",path="setup.yml",become="root")]))

# Node imp
node_imp = request.RawPC('imp')
node_imp.hardware_type = params.HWTYPE
node_imp.disk_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//' + params.IMAGE
node_imp.Site('Site 1')
#
node_imp.Desire('?+disk_nonsysvol','1')
bs0 = node_imp.Blockstore('fs', '/disk2')
bs0.placement = 'NONSYSVOL'

node_imp.addService(pg.Execute(shell="sh",command=HEAD_CMD))

node_imp.bindRole(RoleBinding("setup"))

request.setRoutingStyle('static')

# Print the generated rspec
pc.printRequestRSpec(request)
